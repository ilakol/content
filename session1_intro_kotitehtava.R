#' ---
#' title: "Kotitehtävä 1"
#' author: ilakol
#' output:
#'   html_document:
#'  #   toc: true
#'  #   toc_float: true
#'     number_sections: yes
#'     code_folding: show
#' ---

#' [Linkki kotitehtävän lähdekoodiin gitlab:ssa](https://gitlab.com/utur2016/content/raw/master/session1_intro_kotitehtava.R)

#+ setup
library(knitr)
opts_chunk$set(list(echo=TRUE,eval=FALSE,cache=FALSE,warning=TRUE,message=TRUE))

#' # Tiedostojärjestelmäfunktiot
#' 
#' R:ssä on funktiot käyttöjärjestelmän tiedostojärjestelmän käyttöön, kuten tiedostojen luomiseen (`file.create()`) 
#' kansioiden luomiseen (`dir.create()`).
#' 
#' **Kysymys:** *Millä komennolla luot nykyisen työhakemistoon kansion `aineistot`?*
#+ vastaus11
default_answer(11)

#' **Kysymys:** *Millä komennolla luot kansioon `aineistot` tiedoston 'muistiinpanot.txt'?*
#+ vastaus12
default_answer(12)
default_answer(121)

#' **Kysymys:** *Millä komennolla kopioit kansiossa `aineistot` olevan tiedoston 'muistiinpanot.txt' samaan kansioon nimellä 'muistiinpanot.md'?*
#+ vastaus13
default_answer(13)

#' # Datarakenteiden perusteet
#' 
#' Tällä kurssilla käsittelemme R:n kymmenistä ellei sadoista datarakenteista ainoastaan vektoreita ja data.frameja (tibblejä)
#' 
#' **Kysymys:** Millä komennolla luot numeerisen vektorin nimeltä `numerot`, jossa on kokonaisluvut väliltä 10 - 20?
#+ vastaus21
default_answer(21)

#' **Kysymys:** Millä komennolla luot kirjainvektorin `pohjoismaat` (character vector), 
#' jonka elementteinä ovat pohjoismaiden nimet suomeksi kirjoitettuna?
#+ vastaus22
default_answer(22)
default_answer(221)
default_answer(222)

#' **Kysymys:** Miten luot pienen data.framen, jossa sarakkeiden niminä ovat `etunimi`, `sukunimi`,`puolue` ja `titteli` ja 
#' kolmella rivillä hallituspuolueiden puheenjohtajat, joista sarakkeissa vaaditut tiedot. `titteli` viittaa siis 
#' henkilön ministeripestiin hallituksessa.
#+ vastaus23
default_answer(23)

#' # Datatiedoston lataaminen verkosta ja tallentaminen koneelle
#' 
#' Käyttämämme data "[Wages and Education of Young Males](https://vincentarelbundock.github.io/Rdatasets/doc/plm/Males.html)" 
#' löytyy osoitteesta https://vincentarelbundock.github.io/Rdatasets/csv/plm/Males.csv
#' 
#' **Kysymys:** *Miten tallennan ko. tiedoston nimellä `males.csv` kansioon `aineistot`?*
#+ vastaus31
default_answer(31)


#' # Datatiedoston tuominen R:ään
#' 
#' Paikallisessa kansiossa olevan **tekstimuotoisen** datan tuomiseen käytetään 
#' useimmiten `read.table()`-funktiota tai mikäli kyseessä on pilkuilla erotettu .csv tiedosto
#' `read.csv()`-funktiota. Funktio tarvii argumenteikseen polun tiedostoon `path=...`. Lisäksi usein 
#' määritellään lisäargumentit kuten `header=TRUE/FALSE`, `stringsAsFactors=TRUE/FALSE` ja 
#' toisinaan `FileEncoding="Latin1"` jos käytettävä tiedosto on windowsissa luotu ääkkösiä sisältävä data.
#' 
#' **Kysymys:** *Miten tuot edellisessa vaiheessa tallentamasi tiedoston R:ään funktiolla `read.csv()`, jotta
#' objektin nimeksi tulee `malesdata`?*
#+ vastaus41
default_answer(41)

#' # Datan tarkastelu R:ssä
#' 
#' R:ssä on erilaisia funktioita datan kuvailuun kuten `str()` tai `summary()`.
#' 
#' **Kysymys:** *Miten saat konsoliin/päätteeseen näkyville äsken lataamasi `malesdata` aineiston kuusi ensimmäistä riviä?*
#+ vastaus51
default_answer(51)
default_answer(511)
default_answer(512)
default_answer(513)

#' **Kysymys:** *Miten saat konsoliin/päätteeseen näkyville äsken lataamasi `malesdata` aineiston muuttujien luokat (class)?*
#+ vastaus52
default_answer(52)

#' **Kysymys:** *Mikä `malesdata` aineiston tapausten syntymävuoden keskiarvo (muuttuja `year`)?*
#+ vastaus53
default_answer(53)

#' **Kysymys:** *Mikä `malesdata` aineiston tapausten yleisin ammatti (muuttuja `occupation`)?*
#+ vastaus54
default_answer(54)

#' **Kysymys:** *Mikä osuus `malesdata` aineiston tapauksista on naimisissa (muuttuja `married`)?*
#+ vastaus55
default_answer(55)


#' # Datan siivoaminen R:ssä
#' 
#' Datan siivoaminen korostuu sotkuisten "tosielämän" datojen kanssa työskenneltäessä. 
#' Meidän data on valmiiksi käsitelty tutkimusdata, jolloin siivoamisen tarve on pienempi. 
#' Kuitenkin datassa jossain muuttujissa välilyönnit on korvattu alaviivoilla.
#' 
#' **Kysymys:** *Miten korvaat muuttujien `industry` ja `occupation` arvojen alaviivat välilyönneiksi? ja 
#' muutat kaikki kirjaimet pieneksi (lowercase)?*
#+ vastaus61
default_answer(61)

#' **Kysymys:** *Miten muutat muuttujien `industry` ja `occupation` kaikki kirjaimet pieniksi?*
#+ vastaus62
default_answer(62)

#' # Datan muokkaaminen R:ssä
#' 
#' Toisinaan tarvit datasta ryhmätason yhteenvetotietoja. 
#' `dplyr`-paketin `group_by`- ja `summarise` -funktiot ovat näppäriä tässä.
#' 
#' **Kysymys:** *Kuinka saan ammattiryhmittäiset (muuttuja `occupation`) vastaajien määrät 
#' sekä ammattiryhmittäiset koulutusvuosien (`school´) keskiarvon? 
#' Tee tämä yhteenvetodata uudeksi objektiksi `malesdata_sum` (käytetään sitä myöhemmin)*
#+ vastaus71
default_answer(71)

#' **Kysymys:** Ryhmittele `malesdata` siten että saat eri teollisuudenalojen (`industry`)
#' vuosittaiset palkan keskiarvot. Käytä sitten tidyr-paketin spread-funktiota ja luo datasta leveä
#' versio, joka näyttää tältä
#' 
#' | industry     | 1980 | 1981 | 1982 | ...
#' | ------------ | ---- | ---- | ---- | ...
#' | Agricultural | 1.20 | 1.16 | 1.32 | ...
#' | Construction | 1.33 | 1.54 | 1.58 | ...      
#' 
#+ vastaus72
default_answer(72)

#' # Datan visualisoiminen R:ssä
#' 
#' Aikaisemmassa tehtävässä laadit datan nimeltä `malesdata_sum`.
#' 
#' **Kysymys:** *Miten piirrät `ggplot2`-kirjastolla pylväsdiagrammin, 
#' jossa kullekin ammattiryhmälle on tolppansa väritetty vastaajien määrän mukaan ja 
#' jossa kunkin ammattiryhmän tolpan pituus vastaa koulutusvuosien keskiarvoa?*
#+ vastaus81
default_answer(81)

#' **Kysymys:** *Miten piirrät `ggplot2`-kirjastolla viivadiagrammin kustakin vastaajasta,
#' jossa x-akselilla on kokemus (`exper`) ja y-akselilla palkka (`wage`) ja viivan värin määrä se kuuluuko liittoon (`union`)? 
#' (Muista määritellä group-parametriksi tapausten id (`nr`))
#+ vastaus82
default_answer(82)

#' **Kysymys:** *Jatka edellisen kuvaa niin, jaat vastaajat paneeleihin teollisuudenalan (`industry`) mukaan ja 
#' lisäät viivaan läpinäkyvyyttä .5 verran?*
#+ vastaus83
default_answer(83)

